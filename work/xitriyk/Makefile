## 
# \file      Makefile
# \brief     Install all zz project
# \author    S.Panin <dix75@mail.ru>
# \copyright S.Panin, 2014 - 2015 
# \version   v.2.0
# \created   February (the) 25(th), 2015, 12:08 MSK
# \updated   February (the) 25(th), 2015, 12:08 MSK
# \TODO      
## 

USER1=xitriyk
#USER1=dix
HOME1=/home/${USER1}
ZSH_CONFIG=$(HOME1)/.zsh
VIM_CONFIG=$(HOME1)/.vim
PROGRAMS=guake tmux \
	chromium-browser chromium-browser-l10n skype postgresql \
	mercurial subversion vlc smplayer amarok smplayer-themes \
	p7zip-full p7zip-rar lhasa:386 arj rar unrar unace lzma rpm zip kdiff3 krename \
	zsh ksh krusader vim vim-gtk icoutils python gcc g++ python-software-properties \
	build-essential curl m4 ruby texinfo libbz2-dev libcurl4-openssl-dev libexpat-dev libncurses-dev zlib1g-dev \
	apache2 apache2-bin apache2-data libapache2-mod-php5 libaprutil1-dbd-sqlite3 libaprutil1-ldap php5-mysql php5-pgsql


NULL=/dev/null 2> /dev/null

update: update-vim \
		update-zsh

install:install-core \
		install-git \
		install-zsh \
		install-nodejs \
		install-sails \
		install-postgresql \
		install-adminer \
		install-core-end
update:
	update-sails	

install-core:
	@echo "--- Begin install.---"
	@echo " - Update system ..."
	@aptitude update > ${NULL} 
	@echo " - Upgrade system..."
	@yes |aptitude full-upgrade > ${NULL} 
	@echo " - Install neccesary programs ..."
	@yes | aptitude install ${PROGRAMS} > ${NULL} 

install-core-end:
	@echo "--- End install.---"

install-git:
	@echo " - Copying git files ..."
	@sudo -u ${USER1} cp ${PWD}/git/.gitconfig ${HOME1}
	
install-zsh:
	@echo " - Creating ${ZSH_CONFIG} ..."
	@sudo -u ${USER1} rm -R -f ${ZSH_CONFIG}
	@sudo -u ${USER1} mkdir -p $(ZSH_CONFIG)
	@echo " - Cloning zsh-syntax-highlighting project ..."
	@sudo -u ${USER1} git clone git://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CONFIG}/zsh-syntax-highlighting > ${NULL}
	@echo " - Cloning zsh-git-prompt project ..."
	@sudo -u ${USER1} git clone https://github.com/olivierverdier/zsh-git-prompt.git ${ZSH_CONFIG}/zsh-git-prompt > ${NULL}
	@echo " - Copying zsh files ..."
	@cp ${PWD}/zsh/.zshrc ${HOME1}
	@cp ${PWD}/zsh/.zsh/*.* ${HOME1}/.zsh/
	@echo " - Cnanging shell ..."
	@chsh -s /bin/zsh ${USER1}

install-postgresql:
	@echo " - Installing Postgres ..."
	@mkdir /var/lib/postgresql/data
	@chown postgres /var/lib/postgresql/data
	@echo 'PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/lib/postgresql/9.3/bin"' > /etc/environment
	@echo 'PGDATA="/var/lib/postgresql/data"' >> /etc/environment
	@cp ${PWD}/postgresql/postgresql.conf /etc/postgresql/9.3/main/postgresql.conf
	@cp ${PWD}/postgresql/rc.local /etc/rc.local

install-adminer:
	@echo " - Installing Adminer ..."
	@mkdir /usr/share/adminer
	@wget "http://www.adminer.org/latest.php" -O /usr/share/adminer/latest.php  > ${NULL}
	@ln -s /usr/share/adminer/latest.php /usr/share/adminer/adminer.php
	@echo 'PGDATA="/var/lib/postgresql/data"' >> /etc/environment
	@echo "Alias /adminer.php /usr/share/adminer/adminer.php" | sudo tee /etc/apache2/conf-available/adminer.conf
	@a2enconf adminer.conf
	@service apache2 restart

install-nodejs:
	@echo " - Fetching NodeJs ..."
	@wget http://nodejs.org/dist/latest/node-v0.12.2-linux-x64.tar.gz > ${NULL}
	@mv node-*-linux-x64.tar.gz /tmp/node.tar.gz
	@echo " - Installing NodeJs ..."
	@cd /usr/local; tar --strip-components 1 -xzf /tmp/node.tar.gz

install-sails:
	@echo " - Creating Sails ..."
	@npm install -g sails
	@echo " - Creating Bower ..."
	@npm install -g bower
	@echo " - Creating Nodemon ..."
	@npm install -g nodemon
	@cd ${HOME1}/projects/zz/; npm install

update-sails:
	@echo " - Updating Sails ..."
	@npm update -g sails > ${NULL}
	@echo " - Updating Bower ..."
	@npm update -g bower > ${NULL}
	@echo " - Updating Nodemon ..."
	@npm update -g nodemon > ${NULL}
	@cd ${HOME1}/projects/zz/; npm update > ${NULL}

dump:
	@echo " - Dumping the zz to last dump..."
	@sudo -u postgres pg_dump zz | gzip > /tmp/last_db.gz
	@echo " - Dumping the zz to current date dump ..."
	@sudo -u postgres pg_dump zz | gzip > /tmp/db.gz
	@echo " - Saving pictures ..."
	@cd /tmp/; tar -zcvf images.tar.gz ~/projects/zz/.tmp/uploads/
	@cd /tmp/; cp images.tar.gz last_images.tar.gz 
	@echo " - Archiving data ..."
	@cd /tmp/; tar -cf `date +%d_%m_%G`.tar  images.tar.gz db.gz
	@cd /tmp/; rm images.tar.gz; rm db.gz

restore:
	@echo " - Restoring the zz db from last dump..."
	@sudo -u postgres dropdb zz
	@sudo -u postgres createdb zz
	@cat /tmp/last_db.gz | gunzip | sudo -u postgres psql --set ON_ERROR_STOP=on zz
	@cd /tmp; tar -zxvf last_images.tar.gz -C /
	
