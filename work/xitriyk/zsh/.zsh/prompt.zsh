#!/bin/zsh
#
#   @file        prompt.zsh
#   @brief       The main RC file (will be linked to ~/.zshrc)
#   @author      S.Panin
#   @copyright   S.Panin<dix75@mail.ru>
#   @version     v.1.1
#   @create      12 September 2014 y., 13:57
#   @update      01 February  2015 y., 13:21
#   @license     http://opensource.org/licenses/gpl-license.php
#   @TODO
#

autoload -U promptinit
promptinit

function prompt_char {
    if [ $UID -eq 0 ]; then echo "#"; else echo $; fi
}

#PROMPT='%(!.%{$fg_bold[red]%}.%{$fg_bold[green]%}%n@)%m %{$fg_bold[cyan]%}%(!.%1~.%~) $(git_super_status)%_$(prompt_char)%{$reset_color%} '
PROMPT='%(!.%{$fg_bold[red]%}.%{$fg_bold[green]%}%n)$(git_super_status) %{$fg_bold[cyan]%}%(!.%1~.%~) %_$(prompt_char)%{$reset_color%} '

#prompt gentoo   
#prompt adam1
#RPROMPT='$(git_super_status)'

