#!/bin/zsh
#
#   @file        aliasses.zsh
#   @brief       alias definitions which can be edited/modified with 'aedit'
#   @author      S.Panin
#   @copyright   S.Panin<dix75@mail.ru>
#   @version     v.1.3
#   @create      23 September 2014 y., 16:07
#   @update      31 January 2015 y., 14:37
#   @license     http://opensource.org/licenses/gpl-license.php
#   @TODO
#

export EDITOR="vim"
export QEDITOR="gvim"
alias vi="vim"
#alias aedit=" qvim $ZSH_CONFIG/aliases.zsh; source $ZSH_CONFIG/aliases.zsh"
#alias zedit=" qvim $HOME/.zshrc; source $HOME/.zshrc"
#alias qedit=" qvim $HOME/.vimrc"
alias hist='fc -fl 1'

#alias man="unset PAGER; man"
alias grep='grep --color=auto'

# Push and pop directories on directory stack
alias pu='pushd'
alias po='popd'

# Super user
alias _='sudo'
alias sl='nodemon'

##### standard aliases (start with a space to be ignored in history)
# default ls is untouched, except coloring
alias ls=' ls --color=auto'
alias ll=' ls -C -F -h -l --color=always'

alias cd=' cd'
alias ..=' cd ..; ls'
alias ...=' cd ..; cd ..; ls'
alias ....=' cd ..; cd ..; cd ..; ls'
alias cd..='..'
alias cd...='...'
alias cd....='....'
alias mc="sudo krusader"
alias zz="~/projects/zz/"
alias data="~/projects/data/"
alias pp="~/projects/"

alias dump="zz;cd work/xitriyk/; make dump"
alias restore="zz;cd work/xitriyk/; make restore"
alias 2='zz; nodemon'

alias d='dirs -v | head -10'


