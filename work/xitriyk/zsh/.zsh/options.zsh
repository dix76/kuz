#!/bin/zsh
#
#   @file        completion.zsh
#   @brief       Shell options as history size, keyindings, etc
#   @author      S.Panin
#   @copyright   S.Panin<dix75@mail.ru>
#   @version     v.1.0
#   @create      23 September 2014 y., 16:23
#   @update      23 September 2014 y., 16:23
#   @license     http://opensource.org/licenses/gpl-license.php
#   @TODO
#
# 
#

# keybindings Strg+v is your friend :-)
#bindkey "^[[1;5D" .backward-word
#bindkey "^[[1;5C" .forward-word
#bindkey "^[[1;6D" backward-delete-word
#bindkey "^[[1;6C" delete-word
# alt+left (on mac) deletes word
#bindkey "^[" backward-kill-word
# fn-left
#bindkey "^[[H" .backward-word
# fn-right
#bindkey "^[[F" .forward-word

# arrow up/down searches in history if line is already started
#bindkey '^[[A' up-line-or-search
#bindkey '^[[B' down-line-or-search

SAVEHIST=10000
HISTSIZE=12000
HISTFILE=~/.zshhistory
setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt autocd
setopt extendedglob	
bindkey -v

# completion
autoload -Uz compinit
compinit
zstyle :compinstall filename '~/.zshrc'
zstyle ':completion:*:descriptions' format '- %U%B%d%b%u' 
zstyle ':completion:*:warnings' format '%BSorry, no matches for: %d%b'
zstyle ':completion:*' completer _expand _complete _ignored                                  
zstyle ':completion:*' group-name ''                                                         
zstyle ':completion:*' list-colors ''                                                        
zstyle ':completion:*' list-prompt '%SAt %p: Hit TAB for more, or the character to insert%s' 
zstyle ':completion:*' max-errors 1                                                          
zstyle ':completion:*' menu select=long-list select=0                                        
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'         
zstyle ':completion:*' use-compctl false                                                     
zstyle ':completion:*' verbose true                                                          
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*' force-list always 
zstyle ':completion:*:*:kill:*:processes' list-colors "=(#b) #([0-9]#)*=$color[cyan]=$color[red]" 

setopt auto_pushd
setopt pushd_ignore_dups
setopt pushdminus
