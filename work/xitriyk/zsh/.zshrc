#   @file        .zshrc
#   @brief       The main RC file (will be linked to ~/.zshrc)
#   @author      S.Panin
#   @copyright   S.Panin<dix75@mail.ru>
#   @version     v.1.3
#   @create      23 September 2014 y., 13:57
#   @update      01 February  2015 y., 13:19
#   @license     http://opensource.org/licenses/gpl-license.php
#   @TODO
#

# Base Directory Specification
export ZSH_CONFIG="$HOME/.zsh"

typeset -ga sources
sources+="$ZSH_CONFIG/options.zsh"
sources+="$ZSH_CONFIG/prompt.zsh"
sources+="$ZSH_CONFIG/aliases.zsh"
sources+="$ZSH_CONFIG/git-aliases.zsh"

# try to include all sources
foreach file (`echo $sources`)
    if [[ -a $file ]]; then
        source $file
    fi
end

PATH=$PATH:/usr/prm/robomongo/bin/
#
# highlights the live command line
source $HOME/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $HOME/.zsh/zsh-git-prompt/zshrc.sh
