/**
 * Symlink
     https://github.com/gruntjs/grunt-contrib-symlink*
 * ---------------------------------------------------------------
 *
 * Minifies css files and places them into .tmp/public/min directory.
 *
 * For usage docs see:
 * 		https://github.com/gruntjs/grunt-contrib-symlink
 */
module.exports = function(grunt) {
    grunt.config.set('symlink', {
        dev: {
            files: [
                {
                    expand: true
                   , overwrite: true
                   , cwd: '.tmp/uploads/' 
                   , src: ['/']
                   , dest: '.tmp/public/uploads'
                }
            ]
        }
        , build: {
            files: [{
                expand: true
                , cwd: '.tmp/uploads/'
                , src: ['/']
                , dest: 'www/uploads'
            }]
        }
    });

	grunt.loadNpmTasks('grunt-contrib-symlink');
};
