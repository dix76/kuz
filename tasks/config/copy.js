/**
 * Copy files and folders.
 *
 * ---------------------------------------------------------------
 *
 * # dev task config
 * Copies all directories and files, exept coffescript and less fiels, from the sails
 * assets folder into the .tmp/public directory.
 *
 * # build task config
 * Copies all directories nd files from the .tmp/public directory into a www directory.
 *
 * For usage docs see:
 * 		https://github.com/gruntjs/grunt-contrib-copy
 */
module.exports = function(grunt) {
	grunt.config.set('copy', {
		dev: {
			files: [{
				expand: true,
				cwd: './assets',
				src: ['**/*.!(coffee|less)'],
				dest: '.tmp/public'
            //} , {
                //expand: true
                //, cwd: '.tmp/uploads/'
                //, src: ['**/*']
                //, dest: '.tmp/public/uploads'
             } , {
                expand: true
                , cwd: './bower_components/lightbox/img/'
                , src: ['**.png', '**.gif']
                , dest: '.tmp/public/img/'
             } , {
                expand: true
                , cwd: './bower_components/font-awesome/fonts/'
                , src: ['**/*']
                , dest: '.tmp/public/fonts'
             } , {
                expand: true
                , cwd: './bower_components/bootstrap/fonts/'
                , src: ['**/*.*f']
                , dest: '.tmp/public/fonts'
          }
          , { '.tmp/public/linker/js/jquery.js':                    './bower_components/jquery/dist/jquery.js' }
          //, { '.tmp/public/linker/js/socket.io.js':                   './bower_components/socket.io-client/socket.io.js' }
          , { '.tmp/public/linker/js/angular.js':                   './bower_components/angular/angular.js' }
          , { '.tmp/public/linker/js/bootstrap.js':                 './bower_components/bootstrap/dist/js/bootstrap.js' }
          //, { '.tmp/public/linker/js/ui-bootstrap.js':                './bower_components/angular-bootstrap/ui-bootstrap.js' }
          //, { '.tmp/public/linker/js/ui-bootstrap-tpls.js':           './bower_components/angular-bootstrap/ui-bootstrap-tpls.js' }
          //, { '.tmp/public/linker/js/angular-socket.io.js':           './bower_components/angular-socket-io/socket.js' }
        , { '.tmp/public/linker/js/bootstrap-select.js':            './bower_components/bootstrap-select/dist/js/bootstrap-select.min.js' }
        , { '.tmp/public/linker/js/bootstrap-typeahead.js':         './bower_components/bootstrap3-typeahead.min.js' }
        , { '.tmp/public/linker/js/bootstrap-select-messages.js':   './bower_components/bootstrap-select/dist/js/i18n/defaults-ru_RU.min.js' }
        , { '.tmp/public/linker/js/nya-bootstrap-select.js':        './bower_components/nya-bootstrap-select/dist/js/nya-bs-select.min.js'}
        , { '.tmp/public/linker/js/jquery.validate.min.js':         './bower_components/jquery-validation/dist/jquery.validate.min.js' }
        , { '.tmp/public/linker/js/jquery.validate.messages_ru.js': './bower_components/jquery-validation/src/localization/messages_ru.js' }
        , { '.tmp/public/linker/js/jasny-bootstrap.js':             './bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js' }
        , { '.tmp/public/linker/js/lightbox.js':                    './bower_components/lightbox/js/lightbox.min.js' }
        , { '.tmp/public/linker/js/jfunctions.js':                  './bower_components/my-jfunctions/dist/jfunctions.js' }


        , { '.tmp/public/styles/lightbox.css':                      './bower_components/lightbox/css/lightbox.css' }
        , { '.tmp/public/styles/lightbox.css':                      './bower_components/lightbox/css/lightbox.css' }
        , { '.tmp/public/styles/nya-bootstrap-select.css':          './bower_components/nya-bootstrap-select/dist/css/nya-bs-select.min.css'}
            ]
		},
		build: {
			files: [{
				expand: true,
				cwd: '.tmp/public',
				src: ['**/*'],
				dest: 'www'
			}]
		}
	});

	grunt.loadNpmTasks('grunt-contrib-copy');
};
