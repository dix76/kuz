/**
* \file      routes.js
* \brief     Route Mappings
* \author    S.Panin <dix75@mail.ru>
* \copyright S.Panin, 2014 - 2015 
* \version   v.2.10
* \created   January (the) 19(th), 2015, 17:40 MSK
* \updated   March   (the) 04(th), 2015, 16:42 MSK
* \TODO      
**/

module.exports.routes = {
    "/*": function(req, res, next) {
        if(req.url.search('.ico') > 0) return next();
        if(req.url.search('.png') > 0) return next();
        if(req.url.search('.jpg') > 0) return next();
        if(req.url.search('.css') > 0) return next();
        if(req.url.search('.gif') > 0) return next();
        if(req.url.search('.js') > 0) return next();
        if(req.url.search('.woff') > 0) return next();

        sails.log.info(req.method, req.url);
        next();
    }
    , '/': {
        view: 'home/index'
    }
    , '/privacypolicy.html': {
        view: 'home/privacypolicy'
    }
    , '/license.html': {
        view: 'home/license'
    }
    , 'GET /auth/logout':              'AuthController.logout'
    , 'GET /auth/login.html':          "AuthController.login"
    , 'GET /auth/registration.html':   "AuthController.registration"

    // user
    , 'GET /company/index.html':       "UserViewController.company_index"
    , 'GET /company/rubric.html':      "UserViewController.company_rubric"
    , 'GET /product/index.html':       "UserViewController.product_index"
    , 'GET /product/one.html':         "UserViewController.product_one"
    , 'GET /product/rubric.html':      "UserViewController.product_rubric"

    // company
    , 'GET /admin/company/index.html': "AdminViewController.company_index"
    , 'GET /admin/company/add.html':   "AdminViewController.company_add"
    , 'GET /admin/company/edit.html':  "AdminViewController.company_edit"

    // product
    , 'GET /admin/product/index.html': "AdminViewController.product_index"
    , 'GET /admin/product/add.html':   "AdminViewController.product_add"
    , 'GET /admin/product/edit.html':  "AdminViewController.product_edit"
};

// cid - company
// pid - product
