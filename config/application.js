Object.defineProperty(global, '__stack', {
    get: function() {
        var orig = Error.prepareStackTrace;

        Error.prepareStackTrace = function(_, stack) {
            return stack;
        };

        var err = new Error;
        Error.captureStackTrace(err, arguments.callee);

        var stack = err.stack;
        Error.prepareStackTrace = orig;
        return stack;
    }
});

Object.defineProperty(global, '__line', {
    get: function() {
        return __stack[1].getLineNumber();
    }
});

Object.defineProperty(global, '__function', {
    get: function() {
        return __stack[1].getFunctionName();
    }
});

Object.defineProperty(global, '__time', {
    get: function() {
        var updateNumber = function(num) {
            return num < 10 ? "0" + num : num;
        }

        var date = new Date()
        ,d   = date.getUTCDate()
        ,m   = date.getUTCMonth()
        ,h   = date.getUTCHours()
        ,mi  = date.getUTCMinutes()
        ,s   = date.getUTCSeconds();

    return "" + updateNumber(d) + "." + updateNumber(m) + "." + date.getUTCFullYear() + " " + updateNumber(h) + ":" + updateNumber(mi) + ":" + updateNumber(s);
    }
});

Object.defineProperty(global, '__fun', {
    get: function() {
        return  {
           "fun" : __stack[1].getFunctionName()
           ,"line" : __stack[1].getLineNumber()
           ,"file" : __filename
           ,"time" : __time
        }
    }
});

