var zz = {
    undef: function(name) {
        return name === undefined || name === null;
    }
    ,isArray: function(val) {
        return Object.prototype.toString.apply(val) === '[object Array]';
    }
    , isObject: function(val) {
        return Object.prototype.toString.apply(val) === '[object Object]';
    }
    ,undefLine: function(arr) {
        if(zz.undef(arr)) return true;
        var result = arr;
        for(var i = 1; i !== arguments.length; ++i) {
            result = result[arguments[i]];
            if(zz.undef(result)) return true;
        }
        return false;
    }
    , deep: function(o, arr) {
        var result = {}
            , e;
        if(zz.undef(o)) return result;
            
        if(zz.undef(arr)) {
            for(var i in o)
                result[i] = o[i];
            return result;
        }

        for(var i =0; i !== arr.length; ++i) {
            e = arr[i];
            if(!zz.undef(o[e])) result[e] = o[e];
        }
        return result;
    }
    , createUrl: function(url, params) {
        if(!params)  return url;
        return url + '?' + $.param(params);
    }
    ,  updateImagePath: function(name, size) { 
        if(zz.undef(name)) return "http://placehold.it/" + (size? size: "200x150");
            if(name.files && name.files[0]) {
                var file = name.files[0].file;
                return '/uploads/' + file.substr(file.lastIndexOf('/') + 1); 
            }
        return "";
    }    
    /**
     *  \code
            var params = zz.location();
     *  \endcode
    **/
    , location: function () {
        var result = {}
            , param
            , params = window.location.search.split(/\?|\&/);

        params.forEach( function(it) {
            if (!zz.undef(it)) {
                param = it.split("=");
                result[param[0]] = param[1];
            }
        });
        return result;
    }
    , dir: {}           // directives
    , filter: {}        // filters
    , Pagination : {}   // pagination
};
function modalHide(aDialog) {
    $(aDialog).modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
}
function validateObject(aRules) {
    return {
          errorClass: "panel-danger"
        , validClass: "panel-success" 
        , errorElement: "em"
        , rules: aRules 
        , highlight: function(element, errorClass) {
            $(element).fadeOut(function() { $(element).fadeIn(); });
            $(element).addClass("panel-danger");
        }
    };
}


/**
 * \code
 *    !{JSON.stringify(products)})
 * \endcode
 **/
/**
 * \code
 *        if pagination
 *            .row
 *                nav.text-center
 *                    ul.pagination(zz-pagination total="#{pagination.total}", pg="#{pagination.pg}", url="/product/index.html?cid=#{pagination.cid}&", lm="#{pagination.lm}")
 * 
 *        var app = angular.module('zz', ['zz.dir.pagination']);
 * \endcode
**/
zz.dir.pagination = angular.module("zz.dir.pagination",[]);  
zz.dir.pagination.directive('zzPagination', function () {
    return {
        restrict: 'EA',
        compile: function (elem, attrs) {
            var creator = (function (attrs) {
                var pg_      = 1
                    , lm_    = 10
                    , url_   = ""
                    , html_  = ""
                    , count_ = 0;

                function undef(name) {
                    return name === undefined || name === null;
                }
                function setLt(pg) {
                    html_ += "<li><a href='" + getUrl(pg - 1) + "'><b>&lt;</b></a></li>";
                }
                function setGt(pg) {
                    html_ += "<li><a href='" + getUrl(pg + 1) + "'><b>&gt;</b></a></li>";
                }
                function createUrl(url) {
                    if(url.indexOf('?') !== -1) {
                        if(url.indexOf('&') === url[url.length -1]) return url;
                        return url + '&';
                    }
                    return url + '?';
                }
                function getUrl(pg) {
                    return url_ + "pg=" + pg + "&lm=" + lm_;
                }
                function setDots() {
                    html_ += "<li class='disabled'><a>...</a></li>";
                }
                function setPage(pg) {
                    html_ += "<li><a href='" + getUrl(pg) + "'>" + pg + "</a></li>";
                }
                function setFirstPage() {
                    if(1 === pg_) setCurrentPage(1);
                    else setPage(1);
                }
                function setCurrentPage(pg) {
                    html_ += "<li class='active'><a>" + pg + "</a></li>";
                }
                return function (attrs) {
                      var total = (!undef(attrs.total)? parseInt(attrs.total, 10) : 0);
                      pg_    = (!undef(attrs.pg) ? parseInt(attrs.pg, 10) : 1);
                      lm_    = (!undef(attrs.lm) ? parseInt(attrs.lm, 10) : 10);
                      url_   = createUrl(attrs.url);
                      count_ = Math.ceil(total / lm_);


                    if(pg_ > 1) setLt(pg_);
                    setFirstPage();

                    if(pg_ === 1) {
                        if(count_ > 1) setPage(2);
                        if(count_ === 4) setPage(3);
                        else if(count_ > 4) setDots();
                        if(count_ > 2) setPage(count_);
                    }
                    else if(pg_ === 2) {
                        setCurrentPage(2);
                        if(count_ > 2) setPage(3);
                        if(count_ === 5) setPage(4);
                        else if(count_ > 5) setDots();
                        if(count_ > 3) setPage(count_);
                    }
                    else if(pg_ === 3) {
                        setPage(2);
                        setCurrentPage(3);
                        if(count_ > 3) setPage(4);
                        if(count_ === 6) setPage(5);
                        else if(count_ > 6) setDots();
                        if(count_ > 4) setPage(count_);
                    }
                    else if(pg_ === 4) {
                        setPage(2);
                        setPage(3);
                        setCurrentPage(4);
                        if(count_ > 4) setPage(5);
                        if(count_ === 7) setPage(6);
                        else if(count_ > 7) setDots();
                        if(count_ > 5) setPage(count_);
                    }
                    else if(pg_ === count_ - 2){
                        setDots();
                        setPage(count_ - 3);
                        setCurrentPage(count_ - 2);
                        setPage(count_ - 1);
                        setPage(count_);
                    }
                    else if(pg_ === count_ - 1){
                        setDots();
                        setPage(count_ - 2);
                        setCurrentPage(count_ - 1);
                        setPage(count_);
                    }
                    else if(pg_ === count_){
                        setDots();
                        setPage(count_ - 1);
                        setCurrentPage(count_);
                    }
                    else {
                        setDots();
                        setPage(pg_ - 1);
                        setCurrentPage(pg_);
                        setPage(pg_ + 1);
                        setDots();
                        setPage(count_);
                    }
                    if(pg_ < count_) setGt(pg_);
                    return html_;
                }
            })();

            elem.html(creator(attrs));
        }
        , replace:  true
    };
});
/**
 * \code
 *      if flash && flash.errors
 *          zz-alert(errors='#{JSON.stringify(flash.errors)}')
 *
 *          var app = angular.module('zz', ['zz.dir.alert']);
 * \endcode
**/
zz.dir.alert = angular.module("zz.dir.alert",[]);  
zz.dir.alert.directive('zzAlert', function () {
    return {
        restrict: 'E',
        compile: function (elem, attrs) {
                var errors = JSON.parse(attrs.errors);
                if(zz.undef(errors)) return;
                var html = "<div class='alert alert-danger alert-dismissible' role='alert'>" +
                                "<button class='close' type='button' data-dismiss='alert'>"   +
                                    "<span aria-hidden='true'>&times;</span>"  +
                                    "<span class='sr-only'>Close</span>" +
                                "</button>";

                for(var i = 0; i!== errors.length; ++i) {
                    html +="<li>" + errors[i] + "</li>";
                }
                 
                html += "</div>";
                elem.html(html);
        }
        , replace:  true
    };
});
/**
 * \code
 *    zz-error
 *
 *        $rootScope.$broadcast('sendError', data.errors);  
 *         $scope.$on("sendError", function(event, args) {
 *             $scope.errors = args;
 *         });*
 *
 *       var app = angular.module('zz', ['zz.dir.alert']);
 * \endcode
**/
zz.dir.alert.directive('zzError', function () {
    return {
        restrict: 'E',
        compile: function (elem, attrs) {
                var html = "<div class='alert alert-danger alert-dismissible' role='alert' ng-if='errors.length'>" +
                                "<button class='close' type='button' data-dismiss='alert'>"   +
                                    "<span aria-hidden='true'>&times;</span>"  +
                                    "<span class='sr-only'>Close</span>" +
                                "</button>" + 
                                    "<li ng-repeat='e in errors'>{{e}}</li>" +
                            "</div>";
                elem.html(html);
        }
        , replace:  true
    };
});
/**
 *  \brief A directive to auto-collapse long text in elements with the "dd-text-collapse" attribute
 *   \code
        <p zz-text-collapse="100">{{veryLongText}}</p> 
        p(zz-text-collapse="100") {{veryLongText}}
        p(zz-text-limit="100") {{veryLongText}}
        p(zz-text-limit3="100") {{veryLongText}}   // text...
 *   \endcode
**/
zz.dir.text = angular.module("zz.dir.text",[]);  
zz.dir.text.directive('zzTextCollapse', ['$compile', function($compile) {
return {
    restrict: 'A'
    , replace: true
    , link: function(scope, element, attrs) {
        scope.collapsed = false;

        // create the function to toggle the collapse
        scope.toggle = function() {
            scope.collapsed = !scope.collapsed;
        };

        // get the value of the dd-collapse-text attribute
        attrs.$observe('zzTextCollapse', function(maxLength) {
            // get the contents of the element
            var text = element.text();

            if (text.length > maxLength) {
                // split the text in two parts, the first always showing
                var firstPart = String(text).substring(0, maxLength)
                    , secondPart = String(text).substring(maxLength, text.length);

                // create some new html elements to hold the separate info
                var firstSpan = $compile('<span>' + firstPart + '</span>')(scope)
                    , secondSpan = $compile('<span ng-if="zz-collapsed">' + secondPart + '</span>')(scope)
                    , moreIndicatorSpan = $compile('<span ng-if="!zz-collapsed">...</span>')(scope)
                    , toggleButton = $compile('<strong><a class="zz-cursor-pointer" ng-click="toggle()"> {{zz-collapsed ? "<<<" : ">>>"}} </a></strong>')(scope);

                // remove the current contents of the element and add the new ones we created
                element.empty();
                element.append(firstSpan);
                element.append(secondSpan);
                element.append(moreIndicatorSpan);
                element.append(toggleButton);
            }
        });
    }
};
}]);
zz.dir.text.directive('zzTextLimit', ['$compile', function($compile) {
return {
    restrict: 'A'
    , replace: true
    , link: function(scope, e, attrs) {
            attrs.$observe('zzTextLimit', function(maxLength) {
                e.html(e.text().substring(0, maxLength));
            });
        }
    };
}]);
zz.dir.text.directive('zzTextLimit3', ['$compile', function($compile) {
return {
    restrict: 'A'
    , replace: true
    , link: function(scope, e, attrs) {
            attrs.$observe('zzTextLimit3', function(maxLength) {
                if(e.text().length < maxLength) return;
                e.html(e.text().substring(0, maxLength) + '...');
            });
        }
    };
}]);
/**
 * \code
         var app = angular.module('zz', ['zz.filter.floor']);

        {{data.negative *100 / data.len | floor4 }}%   
 * \endcode
**/
zz.filter.floor = angular.module("zz.filter.floor",[]);  
zz.filter.floor.filter('floor4', function(){
    return function(n){
        if(zz.undef(n) || n == 0) return 0.0;
        return n.toPrecision(4);
    };
});
zz.filter.floor.filter('floor3', function(){
    return function(n){
        if(zz.undef(n) || n == 0) return 0.0;
        return n.toPrecision(3);
    };
});
zz.filter.floor.filter('floor2', function(){
    return function(n){
        if(zz.undef(n) || n == 0) return 0.0;
        return n.toPrecision(2);
    };
});
/**
* \code
*      var png = new zz.Pagination();
*      $scope.pages  = png.createX(pag.pg, pag.lm, pag.total);
*      $scope.pages  = png.createX(pag);
**  or
*      var png = new zz.Pagination();
*      $scope.pages  = png.create(args.pagination);
*       .row
*           nav.text-center
*               ul.pagination
*                   li(ng-repeat="pg in pages" class="{{pg.cls}}")
*                       a(idx="{{pg.id}}" ng-click="onPageClick(pg)") {{pg.vs}}*

*               $scope.onPageClick = function(pg) {
*                   if(zz.undef(pg) || zz.undef(pg.pg)) return;
*                   Service.fetchProducts(pg.pg);
*               }
* \endcode
**/
zz.Pagination = function(url) {
   this.pg_     = 1;
   this.lm_     = 10;
   this.total_    = 0;
   this.pages_  = [];

   return this;
}
zz.Pagination.prototype.updateParams_ = function(pg, lm, total) {
   this.pg_  = !zz.undef(pg) ? parseInt(pg, 10) : 1;
   this.lm_  = !zz.undef(lm) ? parseInt(lm, 10) : 10;
   this.total_ = !zz.undef(total)? parseInt(total, 10) : 0;
   this.count_ = Math.ceil(this.total_ / this.lm_);
   this.pages_ = [];
}
zz.Pagination.prototype.create = function(pag) {
   return this.createX(pag.pg, pag.lm, pag.total);
}
zz.Pagination.prototype.createX = function(pg, lm, total) {
   this.updateParams_(pg, lm, total);

   var pg = this.pg_
       , count = this.count_;

   this.pages_ = [];

   if(pg > 1) this.pages_.push({vs: '<', lm: this.lm_, pg: this.pg_ - 1, cls:'zz-cursor-pointer'});
   this.setFirstPage(1);
   if(pg === 1) {
       if(count > 1) this.setPage(2);
       if(count === 4) this.setPage(3);
       else if(count > 4) this.setDots();
       if(count > 2) this.setPage(count);
   }
   else if(pg === 2) {
       this.setCurrentPage(2);
       if(count > 2) this.setPage(3);
       if(count === 5) this.setPage(4);
       else if(count > 5) this.setDots();
       if(count > 3) this.setPage(count);
   }
   else if(pg === 3) {
       this.setPage(2);
       this.setCurrentPage(3);
       if(count > 3) this.setPage(4);
       if(count === 6) this.setPage(5);
       else if(count > 6) this.setDots();
       if(count > 4) this.setPage(count);

   }
   else if(pg === 4) {
       this.setPage(2);
       this.setPage(3);
       this.setCurrentPage(4);
       if(count > 4) this.setPage(5);
       if(count === 7) this.setPage(6);
       else if(count > 7) this.setDots();
       if(count > 5) this.setPage(count);
   }
   else if(pg === count - 2){
       this.setDots();
       this.setPage(count - 3);
       this.setCurrentPage(count - 2);
       this.setPage(count - 1);
       this.setPage(count);
   }
   else if(pg === count - 1){
       this.setDots();
       this.setPage(count - 2);
       this.setCurrentPage(count - 1);
       this.setPage(count);
   }
   else if(pg === count){
       this.setDots();
       this.setPage(count - 1);
       this.setCurrentPage(count);
   }
   else {
       this.setDots();
       this.setPage(pg - 1);
       this.setCurrentPage(pg);
       this.setPage(pg + 1);
       this.setDots();
       this.setPage(count);
   }
   if(pg < count) this.pages_.push({vs: '>', lm: this.lm_, pg: this.pg_ + 1, cls:'zz-cursor-pointer'});
   return this.pages_;
}
zz.Pagination.prototype.setDots = function() {
   this.pages_.push({vs: '...', cls: 'disabled'});
}
zz.Pagination.prototype.setPage = function(i, arr) {
    this.pages_.push({vs: i, pg: i, lm: this.lm_, cls: 'zz-cursor-pointer'});
}
zz.Pagination.prototype.setCurrentPage = function(arr) {
    this.pages_.push({vs: this.pg_, cls: 'active', pg: this.pg_, lm: this.lm_});
}
zz.Pagination.prototype.setFirstPage = function(i, arr) {
    if(this.pg_ === i) this.setCurrentPage(i);
    else this.setPage(i);
}


