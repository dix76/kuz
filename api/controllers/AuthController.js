/**
 * \file      AuthController.js
 * \brief     The User controller
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2014 - 2015 
 * \version   v.1.2
 * \created   February (the) 01(th), 2015, 01:00 MSK
 * \updated   February (the) 01(th), 2015, 01:08 MSK
 * \TODO      
**/
"use strict";

var passport = require("passport")
    , p2 = require('my-params2')
    , e2 = require('my-errors2')
    , zz = require('my-zz2')
    , async = require('async');

    module.exports = {
        login: function(req,res){
            if(req.session.authenticated) return res.redirect('/');
            return res.view("auth/login");
        }
        , registration: function(req, res, next){
            if(req.session.authenticated) return res.redirect('/');
            return res.view("auth/registration");
        }
        , logout: function (req,res){
            if(!zz.undef(req.session) && !zz.undef(req.session.authenticated)) {
                req.session.destroy();
                req.logout();
            } 
            res.redirect('/');
        }
        , process: function(req,res) {
            if(zz.user.isUserPlus(req)) return e2.json400(req, res, 'Вы уже вошли в систему');

            passport.authenticate('local', function(err, user, info) {
                if (err || !user) return  e2.json(req, res, 'Неверный логин или пароль');

                req.logIn(user, function(err) {
                    if(err) return e2.json500(req, res, __fun, err);

                    req.session.user = user[0]; 
                    req.session.authenticated = user[0]; 
                    return res.redirect('/');
                });
            })(req, res);
        }
        , create: function(req, res, next) {
            var ps =  p2.params(req, ['login', 'password', 'email'], ['area', 'district'])
                , area = req.param('area')
                , district = req.param('district');

            if(zz.user.isUserPlus(req)) return e2.json400(req, res, 'Вы уже зарегистрированы и вошли в систему');
            if(ps.isError) return e2.json400p(req, res, ps.result);
            if(zz.p_undef(area) && zz.p_undef(district)) return e2.json400(req, res, "Необходимо выбрать район города Кузнецка или Кузнецкого района.");
            if(req.param('password') !== req.param('confirm_password')) return e2.json400(req, res, "Пароли не совпадают");

            //ps.result['type'] = 'user';
            ps.result['type'] = 'admin';
            ps.result['name'] = ps.result['login'];

            async.parallel({
                login: function(cb) {
                    User.findOne().where({ login: req.param('login') }).exec(function(err, user) {
                        cb(err, user);
                    });
                }
                , email: function(cb) {
                    User.findOne().where({ email: req.param('email') }).exec(function(err, user) {
                        cb(err, user);
                    });
                }
            }
            , function(err, answer) {
                if(err || !answer)  return e2.json500(req, res, __fun, err);
                if(answer.login)    return e2.json400(req, res, "Пользователь с данным логином уже существует");
                if(answer.email)    return e2.json400(req, res, "Пользователь с данным e-mail уже существует");

                User.create(ps.result, function(err, user) {
                    if(err || !user)  return e2.json500(req, res, __fun, err);

                    res.status(201);
                    req.session.user = user; 
                    req.session.authenticated = user; 
                    res.redirect('/');
                });
            });
        }
        , _config: {
            prefix:      '/api/v1'
            , actions:   true
            , rest:      true
            , shortcuts: false
        }
};
