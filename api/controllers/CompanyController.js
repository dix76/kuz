/**
 * \file      CompanyController.js 
 * \brief     The Company Controller
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2014 - 2015 
 * \version   v.1.2
 * \created   January  (the) 21(th), 2015, 21:55 MSK
 * \updated   February (the) 03(th), 2015, 16:00 MSK
 * \TODO      
**/
'use strict';

var   p2 = require('my-params2')
    , e2 = require('my-errors2')
    , zz = require('my-zz2')
    , async = require('async');

module.exports = {
    create: function(req, res, next) {
        var ps =  p2.params(req, ['title', 'address', 'idescription', 'mode'], ['utitle', 'home_page', 'description'])
            , rubrics = req.param('rubrics')
            , emails  = req.param('emails')
            , phones  = req.param('phones')
            , wwws    = req.param('wwws')
            , images  = req.param('images');

        if(!zz.user.isModeratorPlus(req)) return e2.json401(req, res, 'moderator');
        if(ps.isError) return e2.json400p(req, res, ps.result);
        if(zz.p_undef(rubrics) || !zz.isAObjects(rubrics, ['title', 'index', 'path'])) return e2.json400p(req, res, ['rubrics']);
        if(!zz.p_undef(emails) && !zz.isAObjects(emails, ['title'])) return e2.json400p(req, res, ['emails']);
        if(!zz.p_undef(phones) && !zz.isAObjects(phones, ['title'])) return e2.json400p(req, res, ['phones']);
        if(!zz.p_undef(wwws)   && !zz.isAObjects(wwws, ['title']))   return e2.json400p(req, res, ['wwws']);
        if(!zz.p_undef(images) && !zz.isAObjects(images, ['title', 'path', 'size'])) return e2.json400p(req, res, ['images']);

        Company.create(ps.result, function(err, company) {
            if(err || !company) return e2.json500(req, res, __fun, err);

            company.uids.add(req.session.user.id);

            async.parallel({
                addEmails: function(cb){
                    if(zz.undef(emails)) return cb(null, {});

                    async.map(emails, function(email, cb1) {
                        CompanyEmail.create(email).exec(function(err, e){
                            if(err || !e) return cb1(err, e);

                            company.emails.add(e.id);
                            cb1(err, e) ;
                        });
                    }
                    , function done(err, thisEmails) {
                        cb(err, thisEmails);
                    });
                }
                , addPhones: function(cb){
                    if(zz.undef(phones)) return cb(null, {});

                    async.map(phones, function(phone, cb1) {
                        CompanyPhone.create(phone).exec(function(err, p){
                            if(err || !p) return cb1(err, p);

                            company.phones.add(p.id);
                            cb1(err, p);
                        });
                    }
                    , function done(err, thisPhones) {
                        cb(err, thisPhones);
                    });
                }
                , addWWWs: function(cb){
                    if(zz.undef(wwws)) return cb(null, {});

                    async.map(wwws, function(www, cb1) {
                        CompanyWWW.create(www).exec(function(err, w){
                            if(err || !w) return cb1(err, w);

                            company.wwws.add(w.id);
                            cb1(err, w);
                        });
                    }
                    , function done(err, thisWWWs) {
                        cb(err, thisWWWs);
                    });
                }
                , addRubrics: function(cb){
                    async.map(rubrics, function(rubric, cb1) {
                        CompanyRubric.findOrCreate({path:rubric.path}, rubric).exec(function(err, r){
                            if(err || !r) return cb1(err, r);

                            company.rubrics.add(r.id);
                            cb1(err, r);
                        });
                    }
                    , function done(err, thisRubrics) {
                        cb(err, thisRubrics);
                    });

                }
                , addImages: function(cb){
                    if(zz.undef(images)) return cb(null, {});

                    async.map(images, function(image, cb1) {
                        CompanyImage.create(image).exec(function(err, i){
                            if(err || !i) return cb1(err, i);
                            company.images.add(i.id);
                            cb1(err, i);
                        });
                    }
                    , function done(err, thisImages) {
                        cb(err, thisImages);
                    });
                }
            }, function (err, answer) {
                if(err || !answer)  return e2.json500(req, res, __fun, err);

                company.save(function(err) { 
                    if(err || !answer)  return e2.json500(req, res, __fun, err);
                    return res.json(201, company);
                });
            });
        });
    }
    , upload_image: function (req, res) {
        if(!zz.user.isCompanyPlus(req)) return e2.json401(req, res, 'company');
        // e.g. 0   => infinite
        // 240000   => 4 minutes (240,000 miliseconds)
        // Defaults => 2 minutes.
        res.setTimeout(240000);
        req.file('avatar').upload({ dirname: 'companies', maxBytes: 1000000 }, function (err, uploadedFiles) {
            if(err || zz.undef(uploadedFiles) || zz.undef(uploadedFiles[0]) || zz.undef(uploadedFiles[0].fd)) return e2.json500(req, res, __fun, err);

            var data = uploadedFiles[0]
                , pos = data.fd.search('companies');

            if(pos === -1) return e2.json500(req, res, __fun, 'compamies - не найдено!');

            data.fd = data.fd.substr(pos);
            return res.json(201, data);
        });
    }
    , index: function(req, res, next) {
        var pg = zz.updatePagination(req); 

        Company.find({where: {atom: 'enable'}, limit:pg.lm, skip: pg.skip})
        .populate('rubrics', {atom: 'enable'})
        .populate('images',  {atom: 'enable'})
        .populate('wwws',    {atom: 'enable'})
        .populate('phones',  {atom: 'enable'})
        .populate('emails',  {atom: 'enable'})
        .exec(function(err, companies) {
            if(err) return e2.json500(req, res, __fun, err);
            return res.json({pg: pg, companies: companies});
        });
    }
    , comment_index: function(req, res, next) {
        var cid =  req.param('cid')
            , pg = zz.updatePagination(req); 

        async.parallel({
            company: function(cb){
                Company.findOne(cid).exec(function(err, company){
                    cb(err, company);
                });
            }
            , comments: function(cb){
                CompanyComment.find({atom: 'enable', limit:pg.lm, skip: pg.skip, sort: {createdAt:'desc'}}).populate('uid').exec(function(err, comments){
                    cb(err, comments);
                });
            }
        }
        , function(err, answer) {
            if(err || !answer || !answer.company || !answer.comments)  return e2.json500(req, res, __fun, err);
            return res.json({pg: pg, company: answer.company, comments: answer.comments});
        });
    }
    , comment_create: function(req, res, next) {
        var ps =  p2.params(req, ['uid', 'text', 'rate', 'answer'])
            , cid  =  req.param('cid');

        if(!zz.user.isUserPlus(req)) return e2.json401(req, res, 'user');
        if(ps.isError) return e2.json400p(req, res, ps.result);
        if(zz.p_undef(cid)) return e2.json400p(req, res, ['cid']);


        async.parallel({
            comment: function(cb) {
                CompanyComment.create(ps.result).exec(function(err, comment){
                    cb(err, comment);
                });
            }
            , company: function(cb) {
                Company.findOne(cid).exec(function(err, company){
                    cb(err, company);
                });
            }
        }
        , function(err, answer){
            var company = answer.company
                , comment = answer.comment;
            if(err || !answer || !answer.comment || !answer.company)  return e2.json500(req, res, __fun, err);

            company.comments.add(comment.id);
            company.save(function(err) {
                if(err)  return e2.json500(req, res, __fun, err);

                async.parallel({
                    positive: function(cb){
                        CompanyComment.count().where({cid: cid, rate: 1}).exec(function(err, count){ cb(err, count); });
                    }
                   , count: function(cb){
                        CompanyComment.count({ where: {cid: cid}}).exec(function(err, count){ cb(err, count); });
                    }
                }
                , function(err, answer){
                    if(err || !answer || zz.undef(answer.positive) || zz.undef(answer.count))  return e2.json500(req, res, __fun, err);

                    company.comments_info = {
                        rate: answer.positive / answer.count
                       , total: answer.count 
                       , positive: answer.positive
                       , negative: answer.count - answer.positive 
                    };
                    company.save(function(err) {
                        if(err)  return e2.json500(req, res, __fun, err);
                        return res.json(201, comment);
                    });
                });
            });
        });
    }
    , one: function(req, res, next) {
        var id =  req.param('id');
        if(zz.p_undef(id)) return e2.json400p(req, res, ['id']);

        Company.findOne(id)
        .populate('rubrics', {atom: 'enable'})
        .populate('images',  {atom: 'enable'})
        .populate('wwws',    {atom: 'enable'})
        .populate('phones',  {atom: 'enable'})
        .populate('emails',  {atom: 'enable'})
        .exec(function(err, company) {
            if(err) return e2.json500(req, res, __fun, err);
            return res.json(company);
        });
    }
    , update: function(req, res, next) {
        var ps =  p2.params(req, ['title', 'address', 'idescription', 'mode'], ['utitle', 'home_page', 'description'])
            , rubrics = req.param('rubrics')
            , emails  = req.param('emails')
            , phones  = req.param('phones')
            , wwws    = req.param('wwws')
            , images  = req.param('images')
            , id      = req.param('id');

        if(!zz.user.isCompanyPlus(req)) return e2.json401(req, res, 'company');
        if(ps.isError) return e2.json400p(req, res, ps.result);
        if(zz.p_undef(id)) return e2.json400p(req, res, ['id']);
        if(!zz.p_undef(rubrics) && !zz.isAObjects(rubrics, ['title', 'index', 'path', 'action'])) return e2.json400p(req, res, ['rubrics']);
        if(!zz.p_undef(emails)  && !zz.isAObjects(emails, ['title', 'action'])) return e2.json400p(req, res, ['emails']);
        if(!zz.p_undef(phones)  && !zz.isAObjects(phones, ['title', 'action'])) return e2.json400p(req, res, ['phones']);
        if(!zz.p_undef(wwws)    && !zz.isAObjects(wwws, ['title', 'action'])) return e2.json400p(req, res, ['wwws']);
        if(!zz.p_undef(images)  && !zz.isAObjects(images, ['title', 'path', 'action', 'size'])) return e2.json400p(req, res, ['images']);

        Company.update(id, ps.result, function(err, com) {
            if(err || !com) return e2.json500(req, res, __fun, err);

            Company.findOne(id, function(err, company) {
                if(err || !company) return e2.json500(req, res, __fun, err);

                async.parallel({
                    updateRubrics: function(cb){
                        async.map(rubrics, function(rubric, cb) {
                            if(rubric.action === 'add') {
                                CompanyRubric.findOrCreate({path:rubric.path}, rubric).exec(function(err, r){
                                    if(err || !r) return cb(err, r);
                                    company.rubrics.add(r.id);
                                    cb(err, r) ;
                                });
                            } else if(rubric.action === 'delete') {
                                CompanyRubric.findOne({path:rubric.path}, rubric).exec(function(err, r){
                                    if(err || !r) return cb(err, r);
                                    company.rubrics.remove(r.id);
                                    cb(err, r);
                                });
                            } else cb(null, rubric);
                        }
                        , function done(err, thisRubrics) {
                            cb(err, thisRubrics);
                        });
                    }
                  , updateEmails: function(cb){
                        if(zz.undef(emails)) return cb(null, {});

                        async.map(emails, function(email, cb) {
                            if(email.action === 'add') {
                                CompanyEmail.create(email).exec(function(err, e){
                                    if(err || !e) return cb(err, e);
                                    company.emails.add(e.id);
                                    cb(err, e);
                                });
                            } else if(email.action === 'delete') {
                                CompanyEmail.update(email.id, {atom: 'delete'}).exec(function(err, e){
                                    if(err || !e) return cb(err, e);
                                    company.emails.remove(email.id);
                                    cb(err, e);
                                });
                            } else cb(null, email);
                        }
                        , function done(err, thisEmails) {
                            cb(err, thisEmails);
                        });
                    }
                    ,updatePhones: function(cb){
                        if(zz.undef(phones)) return cb(null, {});

                        async.map(phones, function(phone, cb) {
                            if(phone.action === 'add') {
                                CompanyPhone.create(phone).exec(function(err, p){
                                    if(err || !p) return cb(err, p);
                                    company.phones.add(p.id);
                                    cb(err, p);
                                });
                            } else if(phone.action === 'delete') {
                                CompanyPhone.update(phone.id, {atom: 'delete'}).exec(function(err, p){
                                    if(err || !p) return cb(err, p);
                                    company.phones.remove(phone.id);
                                    cb(err, p);
                                });
                            } else cb(null, phone);
                        }
                        , function done(err, thisPhones) {
                            cb(err, thisPhones);
                        });
                    }
                    ,updateWWWs: function(cb){
                        if(zz.undef(wwws)) return cb(null, {});

                        async.map(wwws, function(www, cb) {
                            if(www.action === 'add') {
                                CompanyWWW.create(www).exec(function(err, w){
                                    if(err || !w) return cb(err, w);
                                    company.wwws.add(w.id);
                                    cb(err, w);
                                });
                            } else if(www.action === 'delete') {
                                CompanyWWW.update(www.id, {atom:'delete'}).exec(function(err, w){
                                    if(err || !w) return cb(err, w);
                                    company.wwws.remove(www.id);
                                    cb(err, w);
                                });
                            } else cb(null, www);
                        }
                        , function done(err, thisWWWs) {
                            cb(err, thisWWWs);
                        });
                    }
                    , updateImages: function(cb){
                        if(zz.undef(images)) return cb(null, {});

                        async.map(images, function(image, cb) {
                            if(image.action === 'add') {
                                CompanyImage.create(image).exec(function(err, i){
                                    if(err || !i) return cb(err, i);
                                    company.images.add(i.id);
                                    cb(err, i);
                                });
                            } else if(image.action === 'delete') {
                                CompanyImage.update(image.id, {atom: 'delete'}).exec(function(err, i){
                                    if(err || !i) return cb(err, i);
                                    company.images.remove(image.id);
                                    cb(err, i);
                                });
                            } else cb(null, image);
                        }
                        , function done(err, thisImages) {
                            cb(err, thisImages);
                        });
                    }
                }, function (err, answer) {
                    if(err || !answer || !answer.updateRubrics || !answer.updateEmails || !answer.updatePhones || !answer.updateWWWs || !answer.updateImages) return e2.json500(req, res, __fun, err);

                    company.save(function(err) { 
                        if(err || !answer)  return e2.json500(req, res, __fun, err);
                        return res.json(201, company);
                    });
                });
            });
        });
    }
    , destroy: function(req, res, next) {
        var id =  req.param('id');
        if(!zz.user.isModeratorPlus(req)) return e2.json401(req, res);
        if(zz.p_undef(id)) return e2.json400p(req, res, ['id']);

        Company.update(id, {atom: 'delete'} , function(err, company) {
            if(err || !company) e2.json500(req, res, __fun, err);
            return res.json(company);   
        });
    }
    , rubrics: function(req, res, next) {
        var path =  req.param('path')
            , id =  req.param('id')
            , query =  "SELECT * " +
                       "FROM company_rubrics " + 
                       "WHERE path = (SELECT path FROM company_rubrics  WHERE id = " + id + ")";

        if(zz.p_undef(path) && zz.p_undef(id)) return e2.json400p(req, res, "Отсутствует параметр - path или id");

        if(id) {
            CompanyRubric.query(query, function(err, rubrics) {
                if(err) return e2.view500(view, req, res, __fun, err);

                rubrics = rubrics.rows;
                return res.json({rubrics: rubrics});
            });
        }
        else {
            CompanyRubric.find({ path: path}).exec(function(err, rubrics) {
                if(err) return e2.json500(req, res, __fun, err);
                return res.json({rubrics: rubrics});
            });
        }
    }
    , _config: {
        prefix:      '/api/v1'
        , actions:   true
        , rest:      true
        , shortcuts: false
    }
};

