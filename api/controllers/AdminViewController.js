/**
 * \file      AdminViewController.js
 * \brief     The Only user view controller
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2014 - 2015 
 * \version   v.1.6
 * \created   January  (the) 21(th), 2015, 21:21 MSK
 * \updated   February (the) 17(th), 2015, 19:03 MSK
 * \TODO      
**/
'use strict';

var zz = require('my-zz2');

module.exports = {
    company_index: function(req, res, next)  {
        if(zz.user.isModeratorPlus(req)) return res.view("company/admin/moderator/index");
        return res.redirect('/');
    }
    , company_add: function(req, res, next)  {
        if(zz.user.isModeratorPlus(req)) return res.view("company/admin/moderator/add");
        return res.redirect('/');
    }
    , company_edit: function(req, res, next)  {
        if(zz.user.isModeratorPlus(req)) return res.view("company/admin/moderator/edit");
        return res.redirect('/');
    }
    , product_index: function(req, res, next)  {
        if(zz.user.isCompany(req)) return res.view("product/admin/company/index");
        if(zz.user.isModeratorPlus(req)) return res.view("product/admin/moderator/index");
        return res.redirect('/');
    }
    , product_add: function(req, res, next)  {
        if(zz.user.isCompany(req)) return res.view("product/admin/company/add");
        if(zz.user.isModeratorPlus(req)) return res.view("product/admin/moderator/add");
        return res.redirect('/');
    }
    , product_edit: function(req, res, next)  {
        if(zz.user.isCompany(req)) return res.view("product/admin/company/edit");
        if(zz.user.isModeratorPlus(req)) return res.view("product/admin/moderator/edit");
        return res.redirect('/');
    }
    , _config: {
        prefix:      '/api/v1'
        , actions:   true
        , rest:      true
        , shortcuts: false
    }
};

