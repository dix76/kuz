/**
 * \file      ProductController.js 
 * \brief     The Params functions provides extracting and adding valid object values
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2014 - 2015 
 * \version   v.2.3
 * \created   December (the) 23(th), 2014, 16:00 MSK
// \updated   March    (the) 03(th), 2015, 19:06 MSK
 * \TODO      
**/
'use strict';

var   p2 = require('my-params2')
    , e2 = require('my-errors2')
    , zz = require('my-zz2')
    , uuid = require('node-uuid')
    , fs = require('fs')
    , async = require('async');


var statistics = function(pid, fun) {
    if(!pid) fun(['Невозможно получить статистику без указания id продукта!'], null);

    async.parallel({
        _0: function(cb){
            ProductComment.count().where({pid: pid}).where({rate: 0}).exec(function(err, count){ cb(err, count); });
        }
        , _1: function(cb){
            ProductComment.count().where({pid: pid}).where({rate: 1}).exec(function(err, count){ cb(err, count); });
        }
        , _2: function(cb){
            ProductComment.count().where({pid: pid}).where({rate: 2}).exec(function(err, count){ cb(err, count); });
        }
        , _3: function(cb){
            ProductComment.count().where({pid: pid}).where({rate: 3}).exec(function(err, count){ cb(err, count); });
        }
        , _4: function(cb){
            ProductComment.count().where({pid: pid}).where({rate: 4}).exec(function(err, count){ cb(err, count); });
        }
       , count: function(cb){
            ProductComment.count({ where: {pid: pid}}).exec(function(err, count){ cb(err, count); });
        }
    }
    , function(err, answer) {
        if(err || !answer) fun(err, null);
        var _5 =  answer.count - answer._0 - answer._1 - answer._2 - answer._3 - answer._4;

        fun(null, {
            total : answer.count
            , rate: ((answer._1 + answer._2 * 2 + answer._3 * 3 + answer._4 * 4 + _5 * 5) / (answer.count))
            , _0 :  answer._0
            , _1 :  answer._1
            , _2 :  answer._2
            , _3 :  answer._3
            , _4 :  answer._4
            , _5 :  _5
        });
    });
}

module.exports = {
    create: function(req, res, next) {
        var ps =  p2.params(req, ['title', 'unit', 'price', 'idescription'], ['description'])
            , cid     = req.param('cid')
            , title   = req.param('title')
            , country = req.param('country')
            , brand   = req.param('brand')
            , infos   = req.param('infos')
            , rubrics = req.param('rubrics')
            , images  = req.param('images');

        if(!zz.user.isCompanyPlus(req)) return e2.json401(req, res, 'company');
        if(ps.isError) return e2.json400p(req, res, ps.result);
        if(zz.p_undef(country)) return e2.json400p(req, res, ['country']);
        if(zz.p_undef(brand)) return e2.json400p(req, res, ['brand']);
        if(zz.p_undef(cid)) return e2.json400p(req, res, ['cid']);
        if(!zz.p_undef(infos) && !zz.isAObjects(infos, ['key', 'value'])) return e2.json400p(req, res, ['infos']);
        if(zz.p_undef(rubrics) || !zz.isAObjects(rubrics, ['title', 'index', 'path'])) return e2.json400p(req, res, ['rubrics']);
        if(!zz.p_undef(images) && !zz.isAObjects(images, ['title', 'path', 'size'])) return e2.json400p(req, res, ['images']);

        async.parallel({
            company: function(cb){
                Company.findOne(cid).exec(function(err, company) {
                    cb(err, company);
                });
            }
            , article: function(cb){
                ProductArticle.findOrCreate({title: title}, {title: title}).exec(function(err, article) {
                    cb(err, article);
                });
            }
            , brand: function(cb){
                ProductBrand.findOrCreate({title: brand}, {title: brand}).exec(function(err, brand) {
                    cb(err, brand);
                });
            }
            , country: function(cb){
                ProductCountry.findOrCreate({title: country}, {title: country}).exec(function(err, country) {
                    cb(err, country);
                });
            }
            , product: function(cb) {
                Product.create(ps.result, function(err, product) {
                    cb(err, product);
                });
            }
        }, function(err, results) {
                var company   = results.company
                    , article = results.article
                    , product = results.product
                    , country = results.country
                    , brand   = results.brand;

                if(err || !company || !article || !product || !country || !brand) return e2.json500(req, res, __fun, err);

                product.uids.add(req.session.user.id);
                product.cid     = company.id;
                product.country = country.id;
                product.brand   = brand.id;
                product.article  = article.id;

                async.parallel({
                    saveProduct: function(cb){
                        company.products.add(product.id);
                        company.save(function(err) {
                            cb(err, null);
                        });
                    }
                    , saveArticle: function(cb){
                        article.products.add(product.id);
                        article.save(function(err) {
                            cb(err, null);
                        });
                    }
                    , saveCountry: function(cb){
                        country.products.add(product.id);
                        country.save(function(err) {
                            cb(err, null);
                        });
                    }
                    , saveBrand: function(cb){
                        brand.products.add(product.id);
                        brand.save(function(err) {
                            cb(err, null);
                        });
                    }
                    , addInfos: function(cb){
                        if(zz.undef(infos)) return cb(null);

                        async.map(infos, function(info, cb1) {
                            ProductInfo.create(info).exec(function(err, i) {
                                if(err || !i) return cb1(err, i);
                                product.infos.add(i.id);
                                cb1(err, i);
                            });
                        }
                        , function done(err, thisInfos) {
                            cb(err, thisInfos);
                        });
                    }
                    , addRubrics: function(cb){
                         async.map(rubrics, function(rubric, cb1) {
                                ProductRubric.findOrCreate({path:rubric.path}, rubric).exec(function(err, r){
                                    if(err || !r) return cb1(err, r);
                                    product.rubrics.add(r.id);
                                    cb1(err, r);
                                });
                            }
                            , function done(err, thisRubrics) {
                                cb(err, thisRubrics);
                            });
                    }
                    , addImages: function(cb){
                        if(zz.undef(images)) return cb(null);

                        async.map(images, function(image, cb1) {
                            ProductImage.create(image).exec(function(err, i) {
                                if(err || !i) return cb1(err, i);
                                product.images.add(i.id);
                                cb1(err, i);
                            });
                        }
                        , function done(err, thisImages) {
                            cb(err, thisImages);
                        });
                    }
                }
                , function(err, answer) {
                    if(err || !answer) return e2.json500(req, res, __fun, err);

                    product.save(function(err) {
                        if(err) return e2.json500(req, res, __fun, err);
                        return res.json(201, results.product);
                    });
                });
        });
     }
    , clone: function(req, res, next) {
        var  pid     = req.param('pid')
            , cid    = req.param('cid');

        if(!zz.user.isCompanyPlus(req)) return e2.json401(req, res, 'company');
        if(zz.p_undef(pid)) return e2.json400p(req, res, 'pid');
        if(zz.p_undef(cid)) return e2.json400p(req, res, 'cid');

        async.parallel({
            company: function(cb){
                Company.findOne(cid).exec(function(err, company) {
                    cb(err, company);
                });
            }
            , product: function(cb){
                Product.findOne(pid)
                .populate('rubrics',   {atom: 'enable'})
                .populate('images',    {atom: 'enable'})
                .populate('infos',     {atom: 'enable'})
                .exec(function(err, product) {
                    cb(err, product);
                });
            }
        }, function(err, results) {
            if(err || !results.company) return e2.json500(req, res, __fun, err);
            if(!results.product) return e2.json400(req, res, "Нельзя клонировать несуществующий продукт");

            var company   = results.company
                , product = results.product
                , infos   = product.infos
                , rubrics = product.rubrics
                , images  = product.images;

                delete product.id;
                delete product.createdAt;
                delete product.updateAt;
                delete product.cid;
                delete product.infos;
                delete product.images;
                product.atom = 'enable';

                Product.create(product, function(err, thisProduct) {
                    if(err || !thisProduct) return e2.json500(req, res, __fun, err);

                    async.parallel({
                        saveProduct: function(cb){
                            company.products.add(thisProduct.id);
                            company.save(function(err) {
                                thisProduct.cid = company.id;
                                cb(err, null);
                            });
                        }
                        , addInfos: function(cb){
                            if(zz.undef(infos)) return cb(null);

                            async.map(infos, function(info, cb1) {
                                ProductInfo.create({key: info.key, value: info.value}).exec(function(err, i) {
                                    if(err || !i) return cb1(err, i);
                                    thisProduct.infos.add(i.id);
                                    cb1(err, i);
                                });
                            }
                            , function done(err, thisInfos) {
                                cb(err, thisInfos);
                            });
                        }
                        , addImages: function(cb){
                            async.map(images, function(image, cb1) {

                                ProductImage.create({path: image.path, size: image.size, title: image.title}).exec(function(err, i) {
                                    if(err || !i) return cb1(err, i);
                                    thisProduct.images.add(i.id);
                                    cb1(err, i);
                                });
                            }
                            , function done(err, thisImages) {
                                cb(err, thisImages);
                            });
                        }
                    }
                    , function(err, answer) {
                        if(err || !answer) return e2.json500(req, res, __fun, err);

                        thisProduct.save(function(err) {
                            if(err) return e2.json500(req, res, __fun, err);
                            return res.json(201, thisProduct);
                        });
                    });
                });
            });
     }
    , one: function(req, res, next) {
        var id =  req.param('id');
        if(zz.p_undef(id)) return e2.json400p(req, res, ['id']);

        Product.findOne(id)
         .populate('rubrics',   {atom: 'enable'})
         .populate('images',    {atom: 'enable'})
         .populate('country',   {atom: 'enable'})
         .populate('brand',     {atom: 'enable'})
         .populate('article',   {atom: 'enable'})
         .populate('infos',     {atom: 'enable'})
        .exec(function(err, product) {
            if(err) return e2.json500(req, res, __fun, err);
            return res.json(product);
        });
    }
    , brands: function(req, res, next) {
        ProductBrand.find().exec(function(err, brands) {
            if(err) return e2.json500(req, res, __fun, err);
            return res.json(brands);
        });
    }
    , countries: function(req, res, next) {
        ProductCountry.find().exec(function(err, countries) {
            if(err) return e2.json500(req, res, __fun, err);
            return res.json(countries);
        });
    }
    , titles: function(req, res, next) {
        // only titles (others not nesseseary) 
        var cid =  req.param('cid')
            , where   = { atom: {'!': [ 'delete'] }};

        if(zz.p_undef(cid) && zz.user.isCompanyPlus(req)) cid = req.session.user.cid;
        if(!zz.p_undef(cid)) where.cid = cid;

        Product.find({ where: where}).exec(function(err, products) {
            if(err) return e2.json500(req, res, __fun, err);
            return res.json(products);
        });
    }
    , index: function(req, res, next) {
        var cid =  req.param('cid')
            , pg = zz.updatePagination(req)
            , where   = { atom: {'!': [ 'delete'] }};

        if(zz.p_undef(cid) && zz.user.isCompanyPlus(req) && req.session.user) cid = req.session.user.cid;
        if(zz.p_undef(cid)) return e2.json400p(req, res, ['cid']);

        where.cid = cid;

        async.parallel({
            products: function(cb){
                Product.find({ where: where, limit: pg.lm, skip: pg.skip})
                 .populate('rubrics',   {atom: 'enable'})
                 .populate('images',    {atom: 'enable'})
                 .populate('country',   {atom: 'enable'})
                 .populate('brand',     {atom: 'enable'})
                 .populate('infos',     {atom: 'enable'})
                .exec(function(err, products) {
                    cb(err, products);
                });
            }
            , count: function(cb){
                Product.count({ where: where}).exec(function(err, count){
                    cb(err, count);
                });
            }
        }, function(err, results) {
            if(err) return e2.json500(req, res, __fun, err);
            pg.total = results.count;
            return res.json({pg: pg, products: results.products});
        });
    }
    , rubrics: function(req, res, next) {
        var path =  req.param('path')
            , id =  req.param('id')
            , query =  "SELECT * " +
                       "FROM product_rubrics " + 
                       "WHERE path = (SELECT path FROM product_rubrics  WHERE id = " + id + ")";

        if(zz.p_undef(path) && zz.p_undef(id)) return e2.json400p(req, res, "Отсутствует параметр - path или id");

        if(id) {
            ProductRubric.query(query, function(err, rubrics) {
                if(err) return e2.view500(view, req, res, __fun, err);

                rubrics = rubrics.rows;
                return res.json({rubrics: rubrics});
            });
        }
        else {
            ProductRubric.find({ path: path}).exec(function(err, rubrics) {
                if(err) return e2.json500(req, res, __fun, err);
                return res.json({rubrics: rubrics});
            });
        }
    }
     , update: function(req, res, next) {
        var ps =  p2.params(req, ['title', 'unit', 'price', 'idescription'], ['infos', 'description'])
            , cid     = req.param('cid')
            , title   = req.param('title')
            , country = req.param('country')
            , brand   = req.param('brand')
            , rubrics = req.param('rubrics')
            , images  = req.param('images')
            , id      = req.param('id');

        if(!zz.user.isCompanyPlus(req)) return e2.json401(req, res, 'company');
        if(ps.isError) return e2.json400p(req, res, ps.result);
        if(zz.p_undef(id)) return e2.json400p(req, res, ['id']);
        if(zz.p_undef(country)) return e2.json400p(req, res, ['country']);
        if(zz.p_undef(brand)) return e2.json400p(req, res, ['brand']);
        if(zz.p_undef(cid)) return e2.json400p(req, res, ['cid']);
        if(!zz.p_undef(rubrics) && !zz.isAObjects(rubrics, ['title', 'index', 'path'])) return e2.json400p(req, res, ['rubrics']);
        if(!zz.p_undef(images) && !zz.isAObjects(images, ['title', 'path', 'size'])) return e2.json400p(req, res, ['images']);

        Product.update(id, ps.result, function(err, com) {
            if(err || !com) return e2.json500(req, res, __fun, err);

            Product.findOne(id, function(err, product) {
                if(err || !product) return e2.json500(req, res, __fun, err);

                async.parallel({
                    updateCountry: function(cb){
                        ProductCountry.findOne(product.country).exec(function(err, c) {
                            if(err || !c) return cb(err, c);
                            if(c.title === country) return cb(null, c);

                            async.series({
                                remove: function(cb1) {
                                    c.products.remove(product.id);
                                    c.save(function(err) { cb1(err, c) });
                                }
                                , create: function(cb1) {
                                    ProductCountry.findOrCreate({title: country}, {title: country}).exec(function(err, c){
                                        if(err || !c) return cb1(err, c);

                                        c.products.add(product.id);
                                        c.save(function(err) { cb1(err, c) });
                                    });
                                }
                            }, function (err, answer) {
                                cb(err, answer);
                            });

                        });
                    }
                    , updateBrand: function(cb){
                        ProductBrand.findOne(product.brand).exec(function(err, b) {
                            if(err || !b) return cb(err, b);
                            if(b.title === brand) return cb(null, b);

                            async.series({
                                remove: function(cb1) {
                                    b.products.remove(product.id);
                                    b.save(function(err) { cb1(err, b) });
                                }
                                , create: function(cb1) {
                                    ProductBrand.findOrCreate({title: brand}, {title: brand}).exec(function(err, b1){
                                        if(err || !b1) return cb1(err, b1);

                                        b1.products.add(product.id);
                                        b1.save(function(err) { cb1(err, b1) });
                                    });
                                }
                            }, function (err, answer) {
                                cb(err, answer);
                            });
                        });
                    }
                    , updateRubrics: function(cb){
                        if(zz.undef(rubrics)) return cb(null, {});

                        async.map(rubrics, function(rubric, cb) {
                            if(rubric.action === 'add') {
                                ProductRubric.findOrCreate({path:rubric.path}, rubric).exec(function(err, r){
                                    if(err || !r) return cb(err, r);
                                    product.rubrics.add(r.id);
                                    product.save(function(err) { cb(err, r) });
                                });
                            } else if(rubric.action === 'delete') {
                                ProductRubric.findOne({path:rubric.path}, rubric).exec(function(err, r){
                                    if(err || !r) return cb(err, r);
                                    product.rubrics.remove(r.id);
                                    product.save(function(err) { cb(err, r) });
                                });
                            } else cb(null, rubric);
                        }
                        , function done(err, thisRubrics) {
                            cb(err, thisRubrics);
                        });
                    }
                    , updateImages: function(cb){
                        if(zz.undef(images)) return cb(null, {});

                        async.map(images, function(image, cb) {
                            if(image.action === 'add') {
                                ProductImage.create(image).exec(function(err, i){
                                    if(err || !i) return cb(err, i);
                                    product.images.add(i.id);
                                    product.save(function(err) { cb(err, i) });
                                });
                            } else if(image.action === 'delete') {
                                ProductImage.update(image.id, {atom: 'delete'}).exec(function(err, i){
                                    if(err || !i) return cb(err, i);
                                    product.images.remove(image.id);
                                    product.save(function(err) { cb(err, i) });
                                });
                            } else cb(null, image);
                        }
                        , function done(err, thisImages) {
                            cb(err, thisImages);
                        });
                    }
                }, function (err, answer) {
                    if(err || !answer || !answer.updateBrand || !answer.updateCountry || !answer.updateRubrics || !answer.updateImages) return e2.json500(req, res, __fun, err);
                    return res.json(200, product);
                });
            });
        });
     }
     , atom: function(req, res, next) {
        var arr = req.param('atoms');
        if(!zz.user.isCompanyPlus(req)) return res.redirect('/');
        if(zz.p_undef(arr)) return e2.json400p(req, res, ['atoms']);

        async.each(arr, function(a, cb) {
                if(!zz.undef(a.id) && !zz.undef(a.atom))  {
                    Product.update(a.id, {atom: a.atom}, function(err, product) {
                        cb();
                    });
                }
             }, function(err) {
                if(err) return e2.json500(req, res, __fun, err);
                return res.json(204);   
        });
    }   
    , upload_image: function (req, res) {
        if(!zz.user.isCompanyPlus(req)) return res.redirect('/');
        
        // e.g. 0   => infinite
        // 240000   => 4 minutes (240,000 miliseconds)
        // Defaults => 2 minutes.
        res.setTimeout(240000);
        req.file('avatar').upload({ dirname: 'products', maxBytes: 1000000 }, function (err, uploadedFiles) {
            if(err || zz.undef(uploadedFiles) || zz.undef(uploadedFiles[0]) || zz.undef(uploadedFiles[0].fd)) return e2.json500(req, res, __fun, err);

            var data = uploadedFiles[0]
                , pos = data.fd.search('products');

            if(pos === -1) return e2.json500(req, res, __fun, 'Параметр products - отсутствует!');

            data.fd = data.fd.substr(pos);
            return res.json(201, data);
        });
    }
    , destroy: function(req, res, next) {
        var id =  req.param('id');

        if(!zz.user.isCompanyPlus(req)) return res.redirect('/');
        if(zz.p_undef(id)) return e2.json400p(req, res, ['id']);

        Product.update(id, {atom: 'delete'}, function(err, product) {
            if(err || !product) return e2.json500(req, res, __fun, err);
            return res.json(product);   
        });
    }   
    , comment_index: function(req, res, next) {
        var pid =  req.param('pid')
            , pg = zz.updatePagination(req); 

        async.parallel({
            product: function(cb){
                Product.findOne(pid).exec(function(err, product){
                    cb(err, product);
                });
            }
            , comments: function(cb){
                ProductComment.find({atom: 'enable', limit:pg.lm, skip: pg.skip, sort: {createdAt:'desc'}}).populate('uid').exec(function(err, comments){
                    cb(err, comments);
                });
            }
        }
        , function(err, answer) {
            if(err || !answer || !answer.product || !answer.comments)  return e2.json500(req, res, __fun, err);
            return res.json({pg: pg, product: answer.product, comments: answer.comments});
        });
    }
    , comment_create: function(req, res, next) {
        var ps =  p2.params(req, ['uid', 'text', 'rate', 'answer'])
            , pid  =  req.param('pid');

        if(!zz.user.isUserPlus(req)) return e2.json401(req, res, 'user');
        if(ps.isError) return e2.json400p(req, res, ps.result);
        if(zz.p_undef(pid)) return e2.json400p(req, res, ['pid']);

        async.parallel({
            comment: function(cb) {
                ProductComment.create(ps.result).exec(function(err, comment){
                    cb(err, comment);
                });
            }
            , product: function(cb) {
                Product.findOne(pid).exec(function(err, product){
                    cb(err, product);
                });
            }
        }
        , function(err, answer){
            var product = answer.product
                , comment = answer.comment;
            if(err || !answer || !answer.comment || !answer.product)  return e2.json500(req, res, __fun, err);

            product.comments.add(comment.id);
            product.save(function(err) {
                if(err)  return e2.json500(req, res, __fun, err);

                statistics(pid, function(err, stats) {
                    if(err || !stats)  return e2.json500(req, res, __fun, err);

                    product.comments_info = stats;
                    product.save(function(err) {
                        if(err)  return e2.json500(req, res, __fun, err);
                        return res.json(201, comment);
                    });
                });
            });
        });
    }
    , _config: {
        actions:     true
        , rest:      true
        , shortcuts: false
        , prefix:    '/api/v1'
    }
};

