/**
 * \file      UserViewController.js 
 * \brief     The Only user view controller
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2014 - 2015 
 * \version   v.1.3
 * \created   January (the) 21(th), 2015, 21:21 MSK
// \updated   March   (the) 04(th), 2015, 16:43 MSK
 * \TODO      
**/
'use strict';

var e2   = require('my-errors2')
    , zz = require('my-zz2')
    , async = require('async');


module.exports = {
    company_index: function(req, res, next) {
        var view = "company/index"
            , pg = zz.updatePagination(req); 

        async.parallel({
            companies: function(cb){
                Company.find({ where: { atom: 'enable' }, limit: pg.lm, skip: pg.skip })
                 .populate('rubrics', {atom: 'enable'})
                 .populate('images',  {atom: 'enable'})
                 .populate('wwws',    {atom: 'enable'})
                 .populate('phones',  {atom: 'enable'})
                 .populate('emails',  {atom: 'enable'})
                .exec(function(err, companies) {
                    cb(err, companies);
                });
            }
           , count: function(cb){
                Company.count().exec(function(err, count){
                    cb(err, count);
                });
            }
        }
        , function(err, result){
            if(err) return e2.view500(view, req, res, __fun, err);

            pg['total'] = result.count;
            return res.view(view, {pg: pg, companies: result.companies});
        });
    }
    , product_index: function(req, res, next) {
        var view = "product/index"
            , cid =  req.param('cid')
            , pg = zz.updatePagination(req); 

        if(zz.p_undef(cid)) return e2.view400p(view, req, res, ['cid']);

        async.parallel({
            products: function(cb){
                Product.find({ where: { cid: cid, atom: 'enable' }, limit: pg.lm, skip: pg.skip })
                 .populate('rubrics',   {atom: 'enable'})
                 .populate('infos',     {atom: 'enable'})
                 .populate('images',    {atom: 'enable'})
                 .populate('country',   {atom: 'enable'})
                 .populate('brand',     {atom: 'enable'})
                .exec(function(err, products) {
                    cb(err, products);
                });
            }
           , company: function(cb){
                Company.findOne({ where: { id: cid, atom: 'enable' }})
                 .populate('rubrics', {atom: 'enable'})
                 .populate('images',  {atom: 'enable'})
                 .populate('wwws',    {atom: 'enable'})
                 .populate('phones',  {atom: 'enable'})
                 .populate('emails',  {atom: 'enable'})
                .exec(function(err, company) {
                    cb(err, company);
                });
            }
           , count: function(cb){
                Product.count({ where: { cid: cid, atom: 'enable' }}).exec(function(err, count){
                    cb(err, count);
                });
            }
        }
        , function(err, answer){
            if(err || !answer || zz.undef(answer.count) || !answer.company || !answer.products ) return e2.view500(view, req, res, __fun, err);

            pg['total'] = answer.count;
            return res.view(view, {pg: pg, company: answer.company, products: answer.products});
        });
    }
    , product_one: function(req, res, next) {
        var view = "product/one"
            , id =  req.param('id');
        if(zz.p_undef(id)) return e2.view400p(view, req, res, ['id']);

        Product.findOne(id)
        .populate('rubrics',   {atom: 'enable'})
        .populate('infos',     {atom: 'enable'})
        .populate('images',    {atom: 'enable'})
        .populate('country',   {atom: 'enable'})
        .populate('brand',     {atom: 'enable'})
        .populate('cid',       {atom: 'enable'})
       .exec(function(err, product) {
            if(err || !product ) return e2.view500(view, req, res, __fun, err);
            return res.view(view, product); 
       });
    }
    , product_rubric: function(req, res, next) {
        var view = "product/rubric"
            , id = req.param('id')
            , pg = zz.updatePagination(req)
            , query =  "SELECT p.*, json_agg(distinct (cp.title)) as cid_title, json_agg(distinct (cp.id)) as cid_id, json_agg(distinct a) as article, json_agg(distinct c) as country, json_agg(distinct b) as brand" +
                       ", json_agg(distinct i.*) as images,  count(*) OVER() as total" +
                       " FROM products p" +
                       " JOIN companies                      cp ON (cp.id = p.cid)" + 
                       " JOIN product_articles               a  ON (a.id = p.article)" + 
                       " JOIN product_countries              c  ON (c.id = p.country)" + 
                       " JOIN product_brands                 b  ON (b.id = p.brand)" + 
                       " LEFT JOIN product_images            i  ON (i.pid = p.id)" +
                       " WHERE p.id IN (SELECT product_rubrics FROM product_rubrics__productrubric_products  r WHERE r.productrubric_products = " + id + ")" + 
                       " GROUP BY p.id"  + 
                       " LIMIT " + pg.lm + " OFFSET "  + pg.skip;

        if(zz.p_undef(id)) return e2.view400p(view, req, res, 'id');

        Product.query(query, function(err, products) {
            if(err) return e2.view500(view, req, res, __fun, err);

            var products = products.rows
                , cid = {id:0, title: 'Неизвестно'};

            if(!zz.undef(products) && !zz.undef(products[0])) {
                cid = {id: products[0].cid_id[0], title:products[0].cid_title[0]}   
                pg.total = products[0].total;
            } else pg.total = 0;

            return res.view(view, {pg: pg, cid: cid, products: products});
        });
    }
    , company_rubric: function(req, res, next) {
        var view = "company/rubric"
            , id = req.param('id')
            , pg = zz.updatePagination(req)
            , query =  " SELECT c.*, json_agg(DISTINCT i.*) as images, count(*) OVER() as total " +
                       " FROM companies c" +
                       " LEFT JOIN company_images i  ON (i.cid = c.id)" +
                       " WHERE c.id IN (SELECT company_rubrics FROM company_rubrics__companyrubric_companies  r WHERE r.companyrubric_companies = " + id + ")" + 
                       " GROUP BY c.id"  + 
                       " LIMIT " + pg.lm + " OFFSET "  + pg.skip;

        if(zz.p_undef(id)) return e2.view400p(view, req, res, 'id');

        Company.query(query, function(err, companies) {
            if(err) return e2.view500(view, req, res, __fun, err);

            companies = companies.rows;
            pg.total = (!zz.undef(companies) && !zz.undef(companies[0])) ? companies[0].total : 0;
            return res.view(view, {pg: pg, companies: companies});
        });
    }
    , _config: {
        prefix:      '/api/v1'
        , actions:   true
        , rest:      true
        , shortcuts: false
    }
};

