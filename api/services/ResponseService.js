/**
 * /api/services/jsonService.js
 *
 * Generic response helper service.
 */
"use strict";

/**
 * Simple service method to send "proper" error message back to client.
 * This is called basically all over the application in cases where error may occur.
 *
 * @param   {Error|{}}  error   Error object
 * @param   {Request}   req     Request object
 * @param   {Response}  res     Response object
 *
 * @returns {*}
 */
exports.error = function(error, req, res) {
    sails.log.error(__filename + ":" + __line + " [Error triggered]");
    sails.log.error(error);

    var errorMessage = new Error();
    errorMessage.message = error.message ? error.message : error;
    errorMessage.status = error.status ? error.status : 500;

    return response.json(errorMessage.status, errorMessage);
};
