/**
 * /api/services/jsonService.js
 *
 * Generic response helper service.
 */
"use strict";

//function __time() {
    //var date = new Date()
        //,d   = date.getUTCDate()
        //,m   = date.getUTCMonth()
        //,h   = date.getUTCHours()
        //,mi  = date.getUTCMinutes()
        //,s   = date.getUTCSeconds();

    //return "" + updateNumber(d) + "." + updateNumber(m) + "." + date.getUTCFullYear() + " " + updateNumber(h) + ":" + updateNumber(mi) + ":" + updateNumber(s);
//}
/**
 * Simple service method to send "proper" error message back to client.
 * This is called basically all over the application in cases where error may occur.
 *
 * @param   {Error|{}}  error   Error object
 * @param   {Request}   req     Request object
 * @param   {Response}  res     Response object
 *
 * @returns {*}
 */
function __error(message, res, status) {
    var mes = new Error();
    mes.message = message ? message : "unknown error";
    mes.status = status ? status : 500;
    return res.json(mes, mes.status);
};

exports.validationError = function(err, res, status) {
    sails.log.error('[' + __time + ']' +  __filename + ":" + __line + " [ValidationError triggered]");
    sails.log.error(err);
    return __error(err, res, status);
};

exports.notCreate = function(err, res, status) {
    sails.log.error('[' + __time + ']' +  __filename + ":" + __line + " [CreationError triggered]");
    sails.log.error(err);
    return __error("Don't create " + err + " item", res, status);
};

exports.notFindError = function(err, res, status) {
    sails.log.error('[' + __time + ']' +  __filename + ":" + __line + " [FindError triggered]");
    sails.log.error(err);
    return __error(err, res, status);
};

exports.notFind = function(err, res, status) {
    sails.log.error('[' + __time + ']' +  __filename + ":" + __line + " [FindError triggered]");
    sails.log.error(err);
    return __error("Don't find " + err + " items", res, status);
};

exports.notUpdatedError = function(err, res, status) {
    sails.log.error('[' + __time + ']' +  __filename + ":" + __line + " [UpdateError triggered]");
    sails.log.error(err);
    return __error(err, res, status);
};

exports.notUpdate = function(err, res, status) {
    sails.log.error('[' + __time + ']' +  __filename + ":" + __line + " [UpdateError triggered]");
    sails.log.error(err);
    return __error("Don't update " + err + " items", res, status);
};

exports.cannotDestroy = function(err, res, status) {
    sails.log.error('[' + __time + ']' +  __filename + ":" + __line + " [DestroyError triggered]");
    sails.log.error(err);
    return __error("Cannot destroy" + err + " items", res, status);
};
