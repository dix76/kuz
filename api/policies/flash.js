"use strict";

module.exports = function(req, res, next) {
    sails.log.verbose(" POLICY - api/policies/flash.js");

    res.locals.flash = {}
    if(!req.session.flash) return next();
    res.locals.flash = _.clone(req.session.flash);
    req.session.flash = {};
    next();
};
