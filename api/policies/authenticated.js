'use strict';

module.exports = function authenticated(req, res, next) {
    sails.log.verbose(" POLICY - api/policies/authenticated.js");

    if (req.session.user) 
        return next();

    return res.send(403);
}
