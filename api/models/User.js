/**
 * \file      User.js 
 * \brief     The model user
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2014 - 2015 
 * \version   v.1.0
 * \created   December (the) 05(th), 2014, 23:46 MSK
 * \updated   February (the) 09(th), 2015, 15:46 MSK
 * \TODO      
**/
var bcrypt = require('bcrypt');

module.exports = {
    schema: true
    , tableName: 'users'
    , attributes: {
        login: {
            type:       'STRING'
            , required: true
            , unique:   true
            , minLength: 3
            , maxLength: 20
        }
        , password: {
            type:       'STRING'
            , required: true
            , minLength: 3
            , maxLength: 20
        }
        , email: {
            type:       'STRING'
            , required: true
            , email:    true
        }
        , area: {
            type:       'STRING'
        }
        , district: {
            type:       'STRING'
        }
        , type:  {
            type:       'STRING'
            , enum:     ['root', 'admin', 'moderator', 'company', 'user']
            , required: true
        }
        , name: {
            type:       'STRING'
            , minLength: 3
            , maxLength: 20
        }
    , toJSON: function() {
          var obj = this.toObject();
          delete obj.password;
          delete obj.confirm_password;
          return obj;
        }
      }
    , beforeCreate: function(user, cb) {
        bcrypt.genSalt(10, function(err, salt) {
            bcrypt.hash(user.password, salt, function(err, hash) {
                if (err) {
                    console.log(err);
                    cb(err);
                } else {
                    user.password = hash;
                    cb(null, user);
                }
            });
        });
    }
};
