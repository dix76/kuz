/**
 * \file      CompanyComment.js 
 * \brief     The model comments for companies
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2014 - 2015 
 * \version   v.1.2
 * \created   February (the) 01(th), 2015, 15:48 MSK
 * \updated   February (the) 17(th), 2015, 15:54 MSK
 * \TODO      
**/

module.exports = {
    schema: true
    , tableName: 'company_comments'
    , attributes: {
        rate: {
              type: "INTEGER"
            , required: true
        }
        , text: {
              type: 'STRING'
            , maxLength: 256
        }
        , answer: {
            model: 'companycomment'
        }
        , atom: {
              type: 'STRING'
            , required: true
            , enum: ['enable', 'disable', 'delete'] 
            , defaultsTo: 'enable'
        }
        , uid: {
            model: 'user'
        } 
        , cid: {
            model: 'company'
            , via: 'comments'
        } 
    }
};
