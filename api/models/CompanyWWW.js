/**
 * \file      CompanyWWW.js 
 * \brief     The model company www
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2014 - 2015 
 * \version   v.1.2
 * \created   January (the) 25(th), 2015, 00:11 MSK
* \updated   February (the) 17(th), 2015, 18:27 MSK
 * \TODO      
**/

module.exports = {
    schema: true
    , tableName: 'company_wwws'
    , attributes: {
        title: {
            type:       'STRING'
            , required: true
        }
        , atom: {
            type: 'STRING'
            , required: true
            , enum: ['enable', 'disable', 'delete'] 
            , defaultsTo: 'enable'
        }
        , cid: {
            model: 'company'
            , via: 'wwws'
        }
    }
};
