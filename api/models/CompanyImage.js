/**
 * \file      CompanyImages.js 
 * \brief     The model company images
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2014 - 2015 
 * \version   v.1.3
 * \created   January (the) 26(th), 2015, 01:08 MSK
 * \updated   February (the) 17(th), 2015, 18:06 MSK
 * \TODO      
**/

module.exports = {
    schema: true
    , tableName: 'company_images'
    , attributes: {
        title: {
            type:       'STRING'
            , required: true
        }
        , path: {
            type:       'STRING'
            , required: true
        }
        , size: {
            type:       'INTEGER'
            , required: true
        }
        , atom: {
            type: 'STRING'
            , required: true
            , enum: ['enable', 'disable', 'delete'] 
            , defaultsTo: 'enable'
        }
        , cid: {
            model: 'company'
            , via: 'images'
        }
    }
};
