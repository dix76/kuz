/**
 * \file      Company.js 
 * \brief     The model company
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2014 - 2015 
 * \version   v.2.6
 * \created   December (the) 05(th), 2014, 23:46 MSK
 * \updated   February (the) 17(th), 2015, 15:53 MSK
 * \TODO      
**/

module.exports = {
    schema: true
    , tableName: 'companies'
    , attributes: {
        title: {
            type:       'STRING'
            , required: true
        }
        , address: {
            type:       'STRING'
            , required: true
        }
        , idescription: {
            type:       'STRING'
            , required: true
        }
        , description: {
            type:       'STRING'
        }
        , utitle: {
            type:       'STRING'
        }
        , home_page: {
            type:       'STRING'
        }
        , mode: {
            type:       'JSON'
            , required: true
        }
        , comments_info: {
             type: "JSON"
            , defaultsTo: {rate: 0.0, total: 0, positive: 0, negative: 0}
        }
        , atom: {
            type: 'STRING'
            , required: true
            , enum: ['enable', 'disable', 'delete'] 
            , defaultsTo: 'enable'
        }
        , comments: {
            collection: 'companycomment'
            , via: 'cid'
            , dominant: true
        }
        , uids: {
            collection:  'user'
            , dominant: true
        }
        , products: {
            collection: 'product'
            , via: 'cid'
            , dominant: true
        }
        , emails: {
            collection: 'companyemail'
            , via: 'cid'
            , dominant: true
        }
        , phones: {
            collection: 'companyphone'
            , via: 'cid'
            , dominant: true
        }
        , wwws: {
            collection: 'companywww'
            , via: 'cid'
            , dominant: true
        }
        , rubrics: {
            collection: 'companyrubric'
            , via: 'companies'
            , dominant: true
        }
        , images: {
            collection: 'companyimage'
            , via: 'cid'
            , dominant: true
        }
    }
};
