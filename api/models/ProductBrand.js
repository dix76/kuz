/**
 * \file      ProductBrand.js 
 * \brief     The model company brand
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2014 - 2015 
 * \version   v.1.2
 * \created   February (the) 01(th), 2015, 15:29 MSK
 * \updated   February (the) 17(th), 2015, 18:30 MSK
 * \TODO      
**/

module.exports = {
    schema: true
    , tableName: 'product_brands'
    , attributes: {
        title: {
            type:       'STRING'
            , required: true
        }
        , atom: {
            type: 'STRING'
            , required: true
            , enum: ['enable', 'disable', 'delete'] 
            , defaultsTo: 'enable'
        }
        , products: {
            collection: 'product'
            , via: 'brand'
            , dominant: true
        }
    }
};
