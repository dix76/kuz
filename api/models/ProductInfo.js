/**
 * \file      ProductInfo.js 
 * \brief     The model informations for a product
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2014 - 2015 
 * \version   v.1.1
 * \created   February (the) 04(th), 2015, 01:46 MSK
 * \updated   February (the) 18(th), 2015, 12:39 MSK
 * \TODO      
**/

module.exports = {
    schema: true
    , tableName: 'product_infos'
    , attributes: {
        key: {
              type: "STRING"
            , required: true
        }
        , value: {
              type: 'STRING'
        }
        , atom: {
              type: 'STRING'
            , required: true
            , enum: ['enable', 'disable', 'delete'] 
            , defaultsTo: 'enable'
        }
        , pid: {
            model: 'product'
            , via: 'infos'
        } 
    }
};
