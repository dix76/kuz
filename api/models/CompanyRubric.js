/**
 * \file      CompanyRubricator.js 
 * \brief     The model company rubricator
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2014 - 2015 
 * \version   v.1.2
 * \created   January (the) 22(th), 2015, 14:46 MSK
 * \updated   February (the) 17(th), 2015, 18:25 MSK
 * \TODO      
**/

module.exports = {
    schema: true
    , tableName: 'company_rubrics'
    , attributes: {
        index: {
            type:       'INTEGER'
            , required: true
        }
        , title: {
            type:       'STRING'
            , required: true
        }
        , path: {
            type:       'STRING'
            , required: true
        }
        , atom: {
            type: 'STRING'
            , required: true
            , enum: ['enable', 'disable', 'delete'] 
            , defaultsTo: 'enable'
        }
        , companies: {
            collection: 'company'
            , via: 'rubrics'
        }
    }
};
