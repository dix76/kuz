/**
 * \file      ProductComment.js 
 * \brief     The model comments for products
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2014 - 2015 
 * \version   v.1.2
 * \created   January (the) 04(th), 2015, 01:46 MSK
 * \updated   January (the) 21(th), 2015, 16:23 MSK
 * \TODO      
**/

module.exports = {
    schema: true
    , tableName: 'product_comments'
    , attributes: {
        rate: {
              type: "INTEGER"
            , required: true
        }
        , text: {
              type: 'STRING'
            , maxLength: 256
        }
        , answer: {
            model: 'productcomment'
        }
        , atom: {
              type: 'STRING'
            , required: true
            , enum: ['enable', 'disable', 'delete'] 
            , defaultsTo: 'enable'
        }
        , uid: {
            model: 'user'
        } 
        , pid: {
            model: 'product'
            , via: 'comments'
        } 
    }
};
