/**
 * \file      ProductCountry.js 
 * \brief     The model company country
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2014 - 2015 
 * \version   v.1.1
 * \created   February (the) 01(th), 2015, 15:25 MSK
 * \updated   February (the) 17(th), 2015, 18:33 MSK
 * \TODO      
**/

module.exports = {
    schema: true
    , tableName: 'product_countries'
    , attributes: {
        title: {
            type:       'STRING'
            , required: true
        }
        , atom: {
            type: 'STRING'
            , required: true
            , enum: ['enable', 'disable', 'delete'] 
            , defaultsTo: 'enable'
        }
        , products: {
            collection: 'product'
            , via: 'country'
            , dominant: true
        }
    }
};
