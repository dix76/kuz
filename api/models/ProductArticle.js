/**
 * \file      ProductArticle.js 
 * \brief     The model company article
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2014 - 2015 
 * \version   v.1.1
 * \created   February (the) 01(th), 2015, 15:34 MSK
 * \updated   February (the) 03(th), 2015, 20:23 MSK
 * \TODO      
**/

module.exports = {
    schema: true
    , tableName: 'product_articles'
    , attributes: {
        title: {
            type:       'STRING'
            , required: true
        }
        , products: {
            collection: 'product'
            , via: 'article'
            , dominant: true
        }
        , atom: {
            type: 'STRING'
            , required: true
            , enum: ['enable', 'disable', 'delete'] 
            , defaultsTo: 'enable'
        }

    }
};
