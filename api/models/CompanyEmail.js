/**
 * \file      CompanyEmail.js 
 * \brief     The model company email
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2014 - 2015 
 * \version   v.1.3
 * \created   January (the) 24(th), 2015, 14:26 MSK
 * \updated   February (the) 17(th), 2015, 15:53 MSK
 * \TODO      
**/

module.exports = {
    schema: true
    , tableName: 'company_emails'
    , attributes: {
        title: {
            type:       'STRING'
            , required: true
        }
        , atom: {
            type: 'STRING'
            , required: true
            , enum: ['enable', 'disable', 'delete'] 
            , defaultsTo: 'enable'
        }
        , cid: {
            model: 'company'
            , via: 'emails'
        }
    }
};
