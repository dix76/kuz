/**
 * \file      ProductRubricator.js 
 * \brief     The model company rubricator
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2014 - 2015 
 * \version   v.1.0
 * \created   January (the) 31(th), 2015, 23:40 MSK
 * \updated   February (the) 17(th), 2015, 18:38 MSK
 * \TODO      
**/

module.exports = {
    schema: true
    , tableName: 'product_rubrics'
    , attributes: {
        index: {
            type:       'INTEGER'
            , required: true
        }
        , title: {
            type:       'STRING'
            , required: true
        }
        , path: {
            type:       'STRING'
            , required: true
        }
        , atom: {
            type: 'STRING'
            , required: true
            , enum: ['enable', 'disable', 'delete'] 
            , defaultsTo: 'enable'
        }
        , products: {
            collection: 'product'
            , via: 'rubrics'
        }
    }
};
