/**
* \file      Product.js 
* \brief     The product model
* \author    S.Panin <dix75@mail.ru>
* \copyright S.Panin, 2014 - 2015 
* \version   v.2.2
* \created   December (the) 30(th), 2014, 23:34 MSK
* \updated   February (the) 18(th), 2015, 12:34 MSK
* \TODO      
**/

module.exports = {
    schema: true
    , tableName: 'products'
    , attributes: {
        title: {
            type: 'STRING'
            , required: true
        }
        , unit: {
            type: 'STRING'
            , required: true
        }
        , price: {
            type: 'STRING'
            , required: true
        }
        , idescription: {
            type: 'STRING'
            , required: true
        }
        , description: {
            type: 'STRING'
        }
        , comments_info: {
             type: "JSON"
            , defaultsTo: {rate: 0.0, total: 0, _1: 0, _2:0, _3:0, _4:0, _5:0}
        }
        , atom: {
            type: 'STRING'
            , required: true
            , enum: ['enable', 'disable', 'delete'] 
            , defaultsTo: 'enable'
        }
        , cid: {
            model: 'company'
            , via: 'products'
        }
        , uids: {
            collection:  'user'
            , dominant: true
        }
        , article: {
            model: 'productarticle'
            , via: 'products'
        }
        , country: {
            model: 'productcountry'
            , via: 'products'
        }
        , brand: {
            model: 'productbrand'
            , via: 'products'
        }
        , infos: {
            collection:  'productinfo'
            , via: 'pid'
            , dominant: true
        }
        , rubrics: {
            collection: 'productrubric'
            , via: 'products'
            , dominant: true
        }
        , comments: {
            collection: 'productcomment'
            , via: 'pid'
            , dominant: true
        }
        , images: {
            collection: 'productimage'
            , via: 'pid'
            , dominant: true
        }
    }
};

